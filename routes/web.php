<?php

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductoController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);


//Categorias

Route::prefix('categorias')->group(function(){

    Route::get('/', [CategoriaController::class, 'index']);

    Route::get('crear-categoria', [CategoriaController::class, 'createCategoria']);

    Route::get('{nombreCategoria}', [CategoriaController::class, 'categoria']);

    
});

//Nos devuelve todos los productos en formato json
Route::prefix('productos')->group(function(){

    Route::get('{categoria?}', [ProductoController::class, 'index']);
    Route::get('create-producto', [ProductoController::class, 'createProducto']);
    Route::get('ver-producto/{producto}', [ProductoController::class, 'verProducto']);
});

Route::get('productos/json', function(){

    $categorias = [
        "Fideos" =>[
            'Molitalia',
            'Fideos Largos',
            'Cabello de angel',
        ],

        "Verduras" => [
            'Tomate',
            'Lechuga',
            'Cebolla'
        ],
        
    ];

    $productos = [];
    foreach ($categorias as $categoriaArray) {
        foreach ($categoriaArray as $producto) {
            $productos[] = $producto;
        }
    }

    return Response::json($productos);
    
});


/**
 * Admin
 * 
 */

 Route::prefix('admin')->group(function(){

    Route::get('/', [AdminController::class, 'home'])->name('home');

   
 
 });